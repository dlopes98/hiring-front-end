// import { SearchModule } from './search/search.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '' , loadChildren: './search/search.module#SearchModule' },
  {path: ':username' , loadChildren: './info/info.module#InfoModule' }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
