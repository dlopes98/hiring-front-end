import { Observable } from 'rxjs';
import {  User_full, Repo } from './../shared/github.models';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ConsumerService } from '../shared/consumer.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  public user : User_full
  public repos : Repo[]
  constructor(
    private route: ActivatedRoute,
    private consumerService: ConsumerService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params)=>{
      this.consumerService.getRepos(params.username).subscribe((repos:Repo[])=> {
        this.repos = repos        
      })
      this.consumerService.getUser(params.username).subscribe((user:User_full)=>{
        this.user= user
      })
    })
  }

}
