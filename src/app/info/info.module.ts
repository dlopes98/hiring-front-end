import { MaterialModule } from './../material/material.module';
import { InfoRoutingModule } from './info-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info.component';
import { SharedModule } from '../shared/shared.module';
import { RepoComponent } from './repo/repo.component';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    CommonModule,
    InfoRoutingModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [InfoComponent, RepoComponent, UserComponent],
  providers:[
    SharedModule
  ]
})
export class InfoModule { }
