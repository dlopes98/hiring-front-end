import { Component, OnInit, Input } from '@angular/core';
import { Repo } from '../../shared/github.models';

@Component({
  selector: 'app-repo',
  templateUrl: './repo.component.html',
  styleUrls: ['./repo.component.scss']
})
export class RepoComponent implements OnInit {
  @Input() public repo : Repo
  constructor() { }

  ngOnInit() {
  }

}
