import { User_full } from './../../shared/github.models';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() public user : User_full
  constructor() { }

  ngOnInit() {
  }

}
