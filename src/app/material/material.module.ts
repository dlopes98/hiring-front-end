import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  MatCheckboxModule,
  MatInputModule, 
  MatButtonModule, 
  MatCardModule, 
  MatSnackBarModule

} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule
  ],
  declarations: [],
  exports:[
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule
  ]
})
export class MaterialModule { }
