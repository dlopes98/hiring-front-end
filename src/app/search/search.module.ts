import { MaterialModule } from './../material/material.module';
import { SearchRoutingModule } from './search-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';

@NgModule({
  imports: [
    CommonModule,
    SearchRoutingModule,
    MaterialModule
  ],
  declarations: [SearchComponent],
  // bootstrap:[SearchComponent]  
})
export class SearchModule { }
