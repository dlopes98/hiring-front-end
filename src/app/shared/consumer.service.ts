import { Router } from '@angular/router';
import { User, User_full } from './github.models';
import { gitHubAPI } from './urls.gitHubApi';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError, retry } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ConsumerService {

  constructor(
    private snack : MatSnackBar,
    private http : HttpClient,
    private router : Router
  ) { }

  public getUser(username: String):Observable<any>{
    return this.http.get(`${gitHubAPI.user}/${username}`)
      .pipe(
        retry(2),
        map( user => user),
        catchError( (err:HttpErrorResponse) => of(this.errorHandler(err,username)))
      )
  }

  public getRepos(username: String) : Observable<any>{
    return this.http.get(`${gitHubAPI.user}/${username}/repos`)
      .pipe(
        retry(2),
        map( repos => repos),
        catchError( (err:HttpErrorResponse) => of(this.errorHandler(err,username)))
      )
  }
  private errorHandler(err:HttpErrorResponse,username:String){
    this.snack.open(`get error: ${username} ${err.statusText}`,"close",{
      duration: 5000
    })
    this.router.navigate([""])
  }

}
