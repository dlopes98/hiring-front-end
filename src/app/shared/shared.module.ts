import { MaterialModule } from './../material/material.module';
import { ConsumerService } from './consumer.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule
  ],
  declarations: [
  ],
  providers:[
    ConsumerService,
    MaterialModule
  ]
})
export class SharedModule { }
